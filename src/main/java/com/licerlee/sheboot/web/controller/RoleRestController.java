package com.licerlee.sheboot.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.licerlee.sheboot.web.domain.Role;
import com.licerlee.sheboot.web.service.RoleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/roles")
public class RoleRestController {


	@Autowired
	public RoleService service;

	
	@GetMapping(value = "")
	public List<Role> list(Model model) {
		List<Role> r = service.findAll();
		log.info("return:"+ r);
		return r;
	}

	@PostMapping(value = "")
	public String postUser(@ModelAttribute Role entity) {
		service.save(entity);
		return "success";
	}

	@GetMapping(value = "/{id}")
	public Role getUser(@PathVariable String id) {
		Role u = service.find(id);
		return u;
	}

	@PutMapping(value = "/{id}")
	public String putUser(@PathVariable String id, @ModelAttribute Role entity) {
		service.save(entity, id);
		return "success";
	}

	@DeleteMapping(value = "/{id}")
	public String deleteUser(@PathVariable String id) {
		service.delete(id);
		return "success";
	}
	
	@RequestMapping(value = "/count")
	public Long count() {
		return service.count();
	}
	
}
