package com.licerlee.sheboot.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.licerlee.sheboot.web.domain.User;
import com.licerlee.sheboot.web.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * Users restful api 标题：基于RBAC的基础权限框架demo 功能： 描述：
 * 
 * @author liwc
 * @date 2018年8月20日 上午10:23:38 @ UserController
 */
@Slf4j
@RestController
@RequestMapping("/users")
public class UserRestController {


	@Autowired
	public UserService userService;

	/**
	 * 处理"/users/"的GET请求，用来获取用户列表
	 * 
	 * @return
	 */
	@GetMapping(value = "")
	public List<User> getUserList(Model model) {

		List<User> r = userService.findAll();
		log.info("users:"+ r);
		return r;
	}

	/**
	 * 处理"/users/"的POST请求，用来创建User
	 * 
	 * @param user
	 * @return
	 */
	@PostMapping(value = "")
	public String postUser(@ModelAttribute User user) {
		userService.save(user);
		return "success";
	}

	/**
	 * 处理"/users/{id}"的GET请求，用来获取url中id值的User信息
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{id}")
	public User getUser(@PathVariable String id) {
		User u = userService.find(id);
		return u;
	}

	/**
	 * 处理"/users/{id}"的PUT请求，用来更新User信息
	 * 
	 * @param id
	 * @param user
	 * @return
	 */
	@PutMapping(value = "/{id}")
	public String putUser(@PathVariable String id, @ModelAttribute User user) {
		userService.save(user, id);
		return "success";
	}

	/**
	 * 处理"/users/{id}"的DELETE请求，用来删除User
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/{id}")
	public String deleteUser(@PathVariable String id) {
		userService.delete(id);
		return "success";
	}
	
	@PostMapping(value="/user")
	public List<User> findByNameAndPasswd(@RequestParam String name, @RequestParam String passwd){
		List<User> user = userService.findByNameAndPasswd(name, passwd);
		return user;
	}
}
