package com.licerlee.sheboot.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.licerlee.sheboot.common.service.AbstractCommonService;
import com.licerlee.sheboot.web.dao.RoleDao;
import com.licerlee.sheboot.web.domain.Role;

@Service
public class RoleService extends AbstractCommonService<Role, String> {

	
	@Autowired
	private RoleDao dao;

	@Autowired
	public void setRoleDao(RoleDao dao) {
		super.setCommonDao(dao);
	}
	
}
