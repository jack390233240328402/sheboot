package com.licerlee.sheboot.web.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.licerlee.sheboot.common.domain.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "sys_user")
public class User extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6701517981227225087L;

	public final static Integer STATUS_DEFAULT = 0; // 不可用

	public final static Integer STATUS_YES = 1; // 可用

	public final static Integer STATUS_NO = 2; // 不可用

	@Column(length = 20, nullable = false)
	private String userName;

	@Column(length = 100, nullable = false)
	private String passwd;

	@Column(length = 20)
	private String realName;

	@Column(length = 1)
	private Integer userStatus;

	@Column(length = 1)
	private Integer userType;

	// @Column(length = 32)
	@ManyToOne
	@JoinColumn(name = "roleId")
	private Role roles;

	@Column(length = 200)
	private String profile;

}