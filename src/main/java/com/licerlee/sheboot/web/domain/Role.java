package com.licerlee.sheboot.web.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.licerlee.sheboot.common.domain.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "sys_role")
public class Role extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(length = 20, nullable = false)
	private String roleName;

	@Column(length = 2)
	private Integer roleOrder = 0;

	@Column(length = 100, nullable = false)
	private String roleDesc = ""; //默认空

	// private Set<SysMenu> menus;

}