package com.licerlee.sheboot.web.dao;

import com.licerlee.sheboot.common.dao.CommonDao;
import com.licerlee.sheboot.web.domain.Role;

public interface RoleDao extends CommonDao<Role, String> {


}
