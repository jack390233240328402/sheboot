package com.licerlee.sheboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShebootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShebootApplication.class, args);
	}
}
